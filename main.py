#!/usr/bin/env python

# solving quadratic equation: ax^2 + bx + c

# importing math functions:
import math

# getting paramteters for a, b and c from the user:
a = float(input("Enter a: "))
b = float(input("Enter b: "))
c = float(input("Enter c: "))

# checking if a is 0:
if a == 0:
    print("This is not a quadratic equation!")

elif a != 0:

    # calculating the discriminant:
    d = (b * b) - 4 * a * c

    # checking if the discriminant is less than 0:
    if d < 0:
        print("This equation has no real solutions!")

    # checking if the discriminant is 0:
    elif d == 0:

        # calculating x:
        x = (-b) / 2 * a
        print(x)

    elif d != 0:

        # calculating x1 and x2:
        x1 = ((-b) + (math.sqrt(d))) / (2 * a)
        x2 = ((-b) - (math.sqrt(d))) / (2 * a)
        print(x1)
        print(x2)
